import 'package:flutter/material.dart';
import 'package:flutter_ui/ThemeModel.dart';
import 'package:provider/provider.dart';

import 'app.dart';

void main() => runApp(ChangeNotifierProvider(
      child: App(),
      create: (BuildContext context) =>
          ThemeModel(theme: MyAppTheme.LIGHT, primaryColor: Colors.cyan),
    ));
