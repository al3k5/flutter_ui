abstract class HomeListItem {
  final String title;
  final String subTitle;

  HomeListItem([this.title, this.subTitle]);
}

class ButtonsItem extends HomeListItem {
  ButtonsItem({String title, String subTitle}) : super(title, subTitle);
}
