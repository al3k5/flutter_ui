import 'package:flutter/material.dart';
import 'package:flutter_ui/buttons/buttons_screen.dart';
import 'package:flutter_ui/home/home_list_item.dart';
import 'package:flutter_ui/settings/settings_screen.dart';
import 'package:flutter_ui/widgets/top_app_bar.dart';

import '../app_data.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var items = List<HomeListItem>();
    items.add(ButtonsItem(
        title: Strings.homeButtons, subTitle: Strings.homeButtonsDescription));

    return Scaffold(
        appBar: TopAppBar(title: Strings.homeTitle),
        body: ListView.builder(
          itemCount: items.length,
          itemBuilder: (context, index) {
            var item = items[index];
            return Card(
                elevation: 8.0,
                child: ListTile(
                  title: Text(item.title),
                  subtitle: Text(item.subTitle),
                  trailing: Icon(Icons.keyboard_arrow_right),
                  onTap: () {
                    if (item is ButtonsItem) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ButtonsScreen()),
                      );
                    }
                  },
                ));
          },
        ));
  }
}
