import 'package:flutter/material.dart';
import 'package:flutter_ui/home/home_screen.dart';
import 'package:provider/provider.dart';

import 'ThemeModel.dart';
import 'app_data.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<ThemeModel>(builder: (context, themeProvider, child) {
      return MaterialApp(
        title: Strings.appTitle,
        theme: themeProvider.getTheme,
        home: HomePage(),
      );
    });
  }
}
