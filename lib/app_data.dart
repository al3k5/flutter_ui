class Strings {
  static const String appTitle = "Flutter components";

  static const String homeTitle = "Home title";
  static const String homeButtons = "Buttons";
  static const String homeButtonsDescription = "Get to know your buttons!";

  static const String settingsTitle = "Settings";
  static const String settingsMenuTitle = "Settings";
  static const String settingsTheme = "Theme";
  static const String settingsPrimaryColor = "Primary color";
  static const String settingsChangePrimary = "Change primary color";
  static const String settingsConfirmChanges = "Confirm changes";

  static const String ok = "Ok";
  static const String cancel = "Cancel";

  static const String buttonsTitle = "Buttons";
}
