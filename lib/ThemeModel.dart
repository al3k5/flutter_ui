import 'package:flutter/material.dart';

class ThemeModel extends ChangeNotifier {
  ThemeData _selectedTheme;
  MyAppTheme _themeValue;
  Color _primaryColorSwatch;

  ThemeModel({MyAppTheme theme, Color primaryColor}) {
    _setSelectedTheme(theme: theme, primaryColorSwatch: primaryColor);
  }

  ThemeData get getTheme => _selectedTheme;

  MyAppTheme get getThemeValue => _themeValue;

  Color get getPrimaryColorSwatch => _primaryColorSwatch;

  // Not exposing setting of theme directly just in case
  void set({MyAppTheme theme, Color primaryColorSwatch}) {
    _setSelectedTheme(theme: theme, primaryColorSwatch: primaryColorSwatch);
    notifyListeners();
  }

  void _setSelectedTheme(
      {MyAppTheme theme, @required Color primaryColorSwatch}) {
    _themeValue = theme;
    _primaryColorSwatch = primaryColorSwatch;
    ThemeData themeData = ThemeData(primarySwatch: primaryColorSwatch);
    if (theme == MyAppTheme.DARK) {
      themeData = ThemeData.dark();
    }
    _selectedTheme = themeData;
  }

  static List<MyAppTheme> themes = [MyAppTheme.LIGHT, MyAppTheme.DARK];
  static List<String> themeLabels = ['Light', 'Dark'];

  static String getThemeLabel(MyAppTheme theme) {
    if (theme == MyAppTheme.LIGHT) {
      return themeLabels[0];
    } else {
      return themeLabels[1];
    }
  }
}

enum MyAppTheme { LIGHT, DARK }
