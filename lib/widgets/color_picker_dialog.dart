import 'package:flutter/material.dart';
import 'package:flutter_material_color_picker/flutter_material_color_picker.dart';
import 'package:flutter_ui/widgets/dialogs.dart';

import '../app_data.dart';

class ColorPickerDialog extends StatefulWidget {
  final Color selectedColor;
  final VoidCallback onCancelled;
  final Function(Color color) onColorChosen;

  ColorPickerDialog(
      {Key key,
      this.selectedColor,
      this.onCancelled,
      @required this.onColorChosen})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ColorPickerDialogState();
  }
}

class _ColorPickerDialogState extends State<ColorPickerDialog> {
  Color _chosenColor;

  @override
  Widget build(BuildContext context) {
    return AppAlertDialog(
        title: "Choose color",
        content: MaterialColorPicker(
            onMainColorChange: (color) {
              _chosenColor = color;
            },
            allowShades: false,
            shrinkWrap: true,
            selectedColor: widget.selectedColor),
        actions: [
          FlatButton(
            child: Text(Strings.cancel),
            onPressed: onCancelled,
          ),
          FlatButton(
              child: Text(Strings.ok),
              onPressed: () {
                if (_chosenColor != null) {
                  widget.onColorChosen(_chosenColor);
                  Navigator.pop(context);
                } else {
                  onCancelled();
                }
              })
        ]);
  }

  void onCancelled() {
    if (widget.onCancelled != null) {
      widget.onCancelled();
    } else {
      Navigator.pop(context);
    }
  }
}
