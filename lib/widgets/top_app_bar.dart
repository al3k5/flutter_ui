import 'package:flutter/material.dart';
import 'package:flutter_ui/settings/settings_screen.dart';

import '../app_data.dart';

class TopAppBar extends StatelessWidget with PreferredSizeWidget {
  final String title;
  final bool overflowEnabled;

  TopAppBar({this.title, this.overflowEnabled = true});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(title),
      actions: getActions(context),
    );
  }

  List<Widget> getActions(BuildContext context) {
    List<Widget> actions = List<Widget>();
    if (overflowEnabled) {
      actions.add(PopupMenuButton(
        onSelected: (value) {
          if (value == 1) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => SettingScreen()));
          }
        },
        itemBuilder: (BuildContext context) {
          return [
            PopupMenuItem(
              value: 1,
              child: Text(Strings.settingsMenuTitle),
            )
          ];
        },
      ));
    }
    return actions;
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
