import 'package:flutter/material.dart';

class AppAlertDialog extends AlertDialog {
  AppAlertDialog(
      {Key key,
      @required String title,
      @required Widget content,
      @required List<Widget> actions})
      : super(
            key: key,
            title: Text(title),
            content: content,
            actions: actions,
            elevation: 24.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))));
}
