import 'package:flutter/material.dart';

import '../app_data.dart';
import 'dialogs.dart';

class RadioButtonDialog extends StatefulWidget {
  final String title;
  final int initialChosenValue;
  final List<String> allValues;
  final VoidCallback onCancelled;
  final Function(int) onValueChosen;

  RadioButtonDialog(
      {Key key,
      @required this.title,
      @required this.allValues,
      this.initialChosenValue,
      this.onCancelled,
      @required this.onValueChosen});

  @override
  State<StatefulWidget> createState() {
    return _RadioButtonDialogState();
  }
}

class _RadioButtonDialogState extends State<RadioButtonDialog> {
  int _chosenValue;

  @override
  Widget build(BuildContext context) {
    if (_chosenValue == null) {
      _chosenValue = widget.initialChosenValue;
    }
    return AppAlertDialog(
        title: widget.title,
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: buildContent(),
        ),
        actions: [
          FlatButton(
            child: Text(Strings.cancel),
            onPressed: onCancelled,
          ),
          FlatButton(
              child: Text(Strings.ok),
              onPressed: () {
                widget.onValueChosen(_chosenValue);
                Navigator.pop(context);
              })
        ]);
  }

  List<RadioListTile> buildContent() {
    List<RadioListTile> content = new List();
    for (int i = 0; i < widget.allValues.length; i++) {
      content.add(RadioListTile(
        title: Text(widget.allValues[i]),
        value: i,
        groupValue: _chosenValue,
        onChanged: onValueChanged,
      ));
    }
    return content;
  }

  void onValueChanged(value) {
    setState(() {
      _chosenValue = value;
    });
  }

  void onCancelled() {
    if (widget.onCancelled != null) {
      widget.onCancelled();
    } else {
      Navigator.pop(context);
    }
  }
}
