import 'package:flutter/material.dart';
import 'package:flutter_material_color_picker/flutter_material_color_picker.dart';
import 'package:flutter_ui/widgets/color_picker_dialog.dart';
import 'package:flutter_ui/widgets/radio_picker_dialog.dart';
import 'package:flutter_ui/widgets/top_app_bar.dart';
import 'package:provider/provider.dart';

import '../ThemeModel.dart';
import '../app_data.dart';

class SettingScreen extends StatefulWidget {
  const SettingScreen({Key key}) : super(key: key);

  @override
  _SettingsState createState() {
    return _SettingsState();
  }
}

class _SettingsState extends State<SettingScreen> {
  Color _primaryColorSwatch;
  MyAppTheme _chosenTheme;

  void onThemeChosen(theme) {
    setState(() {
      _chosenTheme = ThemeModel.themes[theme];
    });
  }

  void onPrimaryColorSwatchChanged(color) {
    setState(() {
      _primaryColorSwatch = color;
    });
  }

  @override
  Widget build(BuildContext context) {
    ThemeModel global = Provider.of<ThemeModel>(context);
    if (_chosenTheme == null) {
      _chosenTheme = global.getThemeValue;
    }
    if (_primaryColorSwatch == null) {
      _primaryColorSwatch = global.getPrimaryColorSwatch;
    }

    // create object to play around with in this screen
    ThemeModel themeModel =
        ThemeModel(theme: _chosenTheme, primaryColor: _primaryColorSwatch);

    return Theme(
        data: themeModel.getTheme,
        child: Scaffold(
            appBar: TopAppBar(
              title: Strings.settingsTitle,
              overflowEnabled: false
            ),
            body: Column(
              children: <Widget>[
                ListTile(
                  title: Text(Strings.settingsTheme),
                  subtitle: Text(ThemeModel.getThemeLabel(_chosenTheme)),
                  onTap: () => showDialog(
                      context: context,
                      builder: (_) => RadioButtonDialog(
                            title: Strings.settingsTheme,
                            allValues: ThemeModel.themeLabels,
                            initialChosenValue:
                                ThemeModel.themes.indexOf(_chosenTheme),
                            onValueChosen: onThemeChosen,
                          )),
                ),
                ListTile(
                  trailing: CircleColor(
                    circleSize: 24.0,
                    color: _primaryColorSwatch,
                  ),
                  title: Text(Strings.settingsPrimaryColor),
                  subtitle: Text(Strings.settingsChangePrimary),
                  onTap: () => showDialog(
                      context: context,
                      builder: (_) => ColorPickerDialog(
                            selectedColor: _primaryColorSwatch,
                            onColorChosen: onPrimaryColorSwatchChanged,
                          )),
                ),
                FlatButton(
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(18.0),
                      side: BorderSide(color: _primaryColorSwatch)),
                  onPressed: () {
                    Provider.of<ThemeModel>(context, listen: false).set(
                        theme: _chosenTheme,
                        primaryColorSwatch: _primaryColorSwatch);
                    Navigator.pop(context);
                  },
                  child: Text(
                    Strings.settingsConfirmChanges.toUpperCase(),
                  ),
                )
              ],
            )));
  }
}
